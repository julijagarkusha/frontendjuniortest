import superstarImg from '../assets/imgs/user-superstar-2x.jpg';

export default {
    id: 'user1',
    name: 'Superstar',
    email: 'Superstar@gmail.com',
    img: superstarImg
  }
