export default [
  {
    id: 'option1',
    name: 'Select your position'
  },
  {
    id: 'option2',
    name: 'Frontend developer'
  },
  {
    id: 'option3',
    name: 'Backend developer'
  },
  {
    id: 'option4',
    name: 'QA'
  },
  {
    id: 'option5',
    name: 'Lead designer'
  }
]