import React, {PureComponent} from 'react';
import Logo from '../Logo/Logo.jsx';
import Menu from '../Menu/Menu.jsx';
import Links from './Links/Links.jsx';
import ContactInfo from './ContactInfo/ContactInfo.jsx';
import Social from './Social/Social.jsx';
import './footer.less';

export default class Footer extends PureComponent {
  render () {
    return (
      <footer>
        <div className='footerNavigation'>
          <Logo />
          <Menu />
        </div>
        <div className='footerInfo'>
          <ContactInfo/>
          <Links />
        </div>
        <div className='footerSocial'>
          <span className='copyright'>© abz.agency specially for the test task</span>
          <Social />
        </div>
      </footer>
    )
  }
}