import React, {PureComponent} from 'react';
import IconMail from './ContactsIcons/IconMail.jsx';
import IconCellphone from './ContactsIcons/IconsCellphone.jsx';
import IconPhone from './ContactsIcons/IconsPhone.jsx';
import './contactInfo.less'

export default class COntactInfo extends PureComponent {
  render () {
    return (
      <div className='footerInfo__contacts'>
        <div className='footerInfo__email footerInfo__contact'>
          <IconMail />
          <span>work.of.future@gmail.com</span>
        </div>
        <div className='footerInfo__cellphoneNumber footerInfo__contact'>
          <IconPhone />
          <span>+38 (050) 789 24 98</span>
        </div>
        <div className='footerInfo__phoneNumber footerInfo__contact'>
          <IconCellphone />
          <span>+38 (095) 556 08 45</span>
        </div>
      </div>
    )
  }
}