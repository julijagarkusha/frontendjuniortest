import React, {PureComponent} from 'react';
import HtmlImageSvg from './HtmlImageSvg.jsx';
import CssImageSvg from './CssImageSvg.jsx';
import JsImageSvg from './JsImageSvg.jsx';
import './aboutMe.less'

export default class AboutMe extends PureComponent {
  render () {
    return (
      <section id='aboutMe' className='aboutMe'>
        <h2>About my relationships with web-development</h2>
        <div className='aboutMe__content'>
          <div className='aboutMe__info infoHTML'>
            <div className='info__htmlImage'>
              <HtmlImageSvg />
            </div>
            <div className='aboutMe__desc'>
              <h3>I'm in love with HTML</h3>
              <p>Hypertext Markup Language (HTML) is the standard markup language for creating web pages and web applications.</p>
            </div>
          </div>
          <div className='aboutMe__info infoCSS'>
            <div className='info__cssImage'>
              <CssImageSvg />
            </div>
            <div className='aboutMe__desc'>
              <h3>CSS is my best friend</h3>
              <p>Cascading Style Sheets (CSS) is a style sheet language used for describing the presentation of a document written in a markup language like HTML.</p>
            </div>
          </div>
          <div className='aboutMe__info infoJS'>
            <div className='info__jsImage'>
              <JsImageSvg/>
            </div>
            <div className='aboutMe__desc'>
              <h3>JavaScript is my passion</h3>
              <p>JavaScript is a high-level, interpreted programming language. It is a language which is also characterized as dynamic, weakly typed, prototype-based
                and multi-paradigm.</p>
            </div>
          </div>
        </div>
      </section>
    )
  }
}