import React, {PureComponent} from 'react';
import './inputText.less'

export default class InputText extends PureComponent {
  constructor (...props) {
    super(...props);
    this.state = {}
  }
  
  render() {
    return (
      <label htmlFor={this.props.inputClass}>
        <input type="text"
               placeholder={this.props.placeholder}
               className={this.props.className}
               onInput={this.props.inputValue}/>
      </label>
    )
  }
}