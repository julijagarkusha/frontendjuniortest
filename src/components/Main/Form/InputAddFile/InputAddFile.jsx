import React, {PureComponent} from 'react';
import './inputAddFile.less'

export default class InputAddFile extends PureComponent {
  constructor (...props) {
    super (...props);
    this.state = {

    }
  }
  render () {
    const {
      fileName,
      fileLoad,
      fileSize
    } = this.props;
    return (
      <div className='addPhoto'>
        <label>
          <span>{fileName !== '' ? `${fileName}` : 'Upload your photo'}</span>
          <input type='file' accept=".jpg" onChange={fileLoad}/>
          <a className='addPhoto__upload' role='button'>Upload</a>
        </label>
      </div>
    )
  }
}

