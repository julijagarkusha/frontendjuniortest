import React, {PureComponent} from 'react';
import LIVR from 'livr';
import InputText from './InputText/InputText.jsx';
import Select from './Select/Select.jsx';
import InputAddFile from './InputAddFile/InputAddFile.jsx';
import './Form.less'

export default class Form extends PureComponent {
  constructor (...props) {
    super(...props);

    this.state = {
      newUser : {},
      userName    : '',
      userMail    : '',
      phoneNumber : '',
      selectValue : 'Select your position',
      imgSize : '',
      imgName : '',
      imgFile: '',
      errorName: false,
      errorMail: false,
      errorMailMessage: 'Укажите ваш email',
      errorNumberMessage: 'Укажите ваш номер телефона',
      errorNumber: false,
      errorSelect: false,
      errorFile: false,
      buttonDisabled: false
    };
  }

    listValue = (event) => {
    const {target} = event;
      this.setState({
        selectValue: event.target.getAttribute('value')
      });
    };

    fileLoad = (event) => {
      const { target } = event;
      this.setState ({
        imgName: target.files[0].name,
        imgSize: target.files[0].size,
        errorFile: false
      });
    };

    formValidate = () => {
      LIVR.Validator.defaultAutoTrim(true);
      const validator = new LIVR.Validator({
        name: 'required',
        email: [ 'required', 'email' ],
        phone: ['required', { max_length : 13 }],
        position: {
          one_of: [
            'Frontend developer',
            'Backend developer',
            'QA',
            'Lead designer',
          ],
        },
        file: [
          'required',
        ],
      });
      const validData = validator.validate(
          {
            name: this.state.userName,
            email: this.state.userMail,
            phone: this.state.phoneNumber,
            position: this.state.selectValue,
            file: this.state.imgSize
          }
        );

      if (validData) {
        this.setState ({
          errorName: false,
          errorMail: false,
          errorNumber: false,
          errorSelect: false,
          buttonDisabled: false,
          newUser: validData
        });
        console.log('newUser', validData);
      } else {
        const errors = validator.getErrors();
        errors.name==='REQUIRED' ? this.setState({
            errorName: true,
          buttonDisabled: true
          }) : '';
        errors.email==='REQUIRED' ? this.setState({
          errorMail: true,
          buttonDisabled: true
        }) : '';
        errors.email==='WRONG_EMAIL' ? this.setState({
            errorMail: true,
            errorMailMessage: 'Вы ввели неправильный email',
          buttonDisabled: true
          }) : '';
        errors.phone==='REQUIRED' ? this.setState({
            errorNumber: true,
          buttonDisabled: true
          }) : '';
        errors.phone==='TOO_LONG' ? this.setState({
            errorNumber: true,
            errorNumberMessage: 'Максимальная длина - 12 цифр',
          buttonDisabled: true
        }) : '';
        errors.position==='NOT_ALLOWED_VALUE' ? this.setState({
          errorSelect: true,
          buttonDisabled: true
        }) : '';
        errors.file==='REQUIRED' ? this.setState({
          errorFile: true,
          buttonDisabled: true
        }) : '';
      }
    };

  render () {
    return (
      <form className="checkIn__form">
        <div className={this.state.errorName ? `checkIn__formRow ${'checkIn__formRowError'}` : 'checkIn__formRow'}>
          <InputText placeholder="Your name"
                     className="checkIn__userName"
                     inputClass="checkIn__userName"
                     inputValue={(event) => {
                       this.setState({
                         userName: event.target.value,
                         errorName: false
                       })
                     }}/>
          {this.state.errorName && <span className="checkIn__formError">Укажите ваше имя</span>}
        </div>
        <div className={this.state.errorMail ? `checkIn__formRow ${'checkIn__formRowError'}` : 'checkIn__formRow'}>
          <InputText placeholder="Your email"
                     className="checkIn__userEmail"
                     inputClass="checkIn__userEmail"
                     inputValue={(event) => {
                       this.setState({
                         userMail: event.target.value,
                         errorMail: false
                       })
                     }}/>
          {this.state.errorMail && <span className="checkIn__formError">{this.state.errorMailMessage}</span>}
        </div>
        <div className={this.state.errorNumber ? `checkIn__formRow ${'checkIn__formRowError'}` : 'checkIn__formRow'}>
          <InputText placeholder="+3 (___) ___ __ __"
                     className="checkIn__userPhone"
                     inputClass="checkIn__userPhone"
                     inputValue={(event) => {
                       this.setState({
                         phoneNumber: event.target.value,
                         errorNumber: false
                       })
                     }}/>
          {this.state.errorNumber && <span className="checkIn__formError">{this.state.errorNumberMessage}</span>}
        </div>
        <div className={this.state.errorSelect ? `checkIn__formRow checkIn__formRowSelect ${'checkIn__formRowError'}` : 'checkIn__formRow checkIn__formRowSelect'}>
          <Select selectedValue={this.state.selectValue}
                  testClick={this.listValue}/>
          {this.state.selectValue==='Select your position'&& this.state.errorSelect && <span className="checkIn__formError">Выберите позицию из выпадающего списка</span>}
        </div>
        <div className={this.state.errorImg ? `checkIn__formRow checkIn__formRowAddFile ${'checkIn__formRowError'}` : 'checkIn__formRow checkIn__formRowAddFile'}>
          <InputAddFile fileLoad={this.fileLoad}
                        fileName={this.state.imgName}
                        fileSize={this.state.imgSize}/>
          <span className={this.state.errorFile ? `checkIn__formError` : 'addFile__desc'}>File format jpg up to 5 MB, the minimum size of 70x70px</span>
        </div>
        <a className={this.state.buttonDisabled ? `checkIn__singOut ${'checkIn__singOutDisabled'}` : "checkIn__singOut"}
           role="button"
           onClick={this.formValidate}>Sing Out</a>
      </form>
    );
  }
}



