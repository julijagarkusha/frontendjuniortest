import React, {PureComponent} from 'react';
import './main.less';
import SingUp from './SingUp/SingUp.jsx';
import AboutMe from './AboutMe/AboutMe.jsx';
import Requirements from './Requirements/Requirements.jsx';
import Register from './Register/Register.jsx';
import Users from './Users/Users.jsx';

export default class Main extends PureComponent {
  render() {
    return (
      <main>
        <SingUp />
        <AboutMe />
        <Requirements />
        <Users />
        <Register />
      </main>
    )
  }
}