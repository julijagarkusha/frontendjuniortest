import React, {PureComponent} from 'react';
import menuItems from '../../mocks/menuItems.js';
import './menu.less'

export default class Menu extends PureComponent {
  render() {
    return (
      <nav className='menu'>
        {
          menuItems.map((menuItem, key) => {
            return (
              <li key={`key-${menuItem.id}`} className='menu__item'>
                <a href={menuItem.linkTo}>
                  <span>{menuItem.name}</span>
                </a>
              </li>
            )
          })
        }
      </nav>
    )
  }
}