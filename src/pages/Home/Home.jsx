import React, {PureComponent} from 'react';
import './index.less';
import Header from '../../components/Header/Header.jsx';
import Main from '../../components/Main/Main.jsx';
import Footer from '../../components/Footer/Footer.jsx';

export default class Home extends PureComponent {
  render() {
    return (
      <div className='content'>
        <Header />
        <Main />
        <Footer />
      </div>
    )
  }
}